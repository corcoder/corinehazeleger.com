var paintingData;
var showCompressedImages = true;

$(document).ready(function(){
    getPaintingData();


    $(".painting").click(function(){

        // get the id of the picture
        var imgId = $(this).find("img").attr("id");
        // pictures have names like "picture_n", so we split the picture name to get n
        var arr = imgId.split("_");
        // get the image number
        var imgNumber = arr[1];

        // THE NUMBER GETS MULTIPLIED BY ONE!!!! THIS IS SUPER IMPORTANT!!!
        // OTHERWISE IT'LL SEE IT AS A STRING AND EVERY NUMBER WILL BE 10 TOO BIG!!!!!!!
        // MULTIPLY IT BY ONE !!!!
        openPhotoSwipe(imgNumber * 1);
        // MULTIPLY IT!!!

    });


});

function getPriceForPainting(painting) {
    return (painting.width + painting.height) * 3;
}

function openPhotoSwipe(imageNumber) {

    //
    // PHOTOSWIPE
    //

    var pswpElement = document.querySelectorAll('.pswp')[0];

    // build items array
    var items = [];

    // add paitingdata to items
    for (var i = 0; i < paintingData.length; i++) {
        var newItem = {
            title: paintingData[i].name + ", " + paintingData[i].type + ", " + paintingData[i].width + "x" + paintingData[i].height,
            src : "img/normal/" + paintingData[i].filename,
            msrc : "img/square_compressed/" + paintingData[i].filename,
            w: paintingData[i].image_width,
            h: paintingData[i].image_height,
        };

        // Add a price to the title if the painting is available
        if (paintingData[i].available) {
            var price = getPriceForPainting(paintingData[i]);
            newItem.title += ", &euro; " + price;
        }

        items[i] = newItem;
    }

    console.log(items);

    // define options (if needed)
    var options = {
        // optionName: 'option value'
        // for example:

        // THIS IS BROKEN, TODO TODO TODO TODO
        index: imageNumber, // start at first slide
        galleryUID: imageNumber,
        getThumbBoundsFn: function(index) {

            // find thumbnail element
            // var thumbnail = document.querySelectorAll('.painting_img')[index];
            // var thumbnail = $("#picture_" + index);
            var thumbnail = document.getElementById("picture_" + index);

            // get window scroll Y
            var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
            // optionally get horizontal scroll

            // get position of element relative to viewport
            var rect = thumbnail.getBoundingClientRect();

            // w = width
            return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};


            // Good guide on how to get element coordinates:
            // http://javascript.info/tutorial/coordinates
        }
    };


    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);

    // Initializes and opens PhotoSwipe
    gallery.init();

}

// place all the correct images in the paintings in the HTML document.
// (by changing the src property)
function placeImagesInPaintings(paintingData) {

    // store the paintingData in a global variable so that we can use it later
    this.paintingData = paintingData;

    // iterate over the paintings in the paintingData ...
    for (var i = 0; i < paintingData.length; i++) {

        // ... and assign the correct image to each of the pictures in the html
        // (uses the compressed version if showCompressedImages is enabled)
        var path = "img/square" + (showCompressedImages ? "_compressed" : "") + "/" + paintingData[i].filename;
        $("#picture_" + i).attr("src", path);
    }

}

function getPaintingData() {
    $.getJSON("js/paintingdata.json").done(function(data) {
        placeImagesInPaintings(data);
    });
}
